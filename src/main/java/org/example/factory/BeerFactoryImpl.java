package org.example.factory;

import org.example.model.Beer;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class BeerFactoryImpl implements BeerFactory{

    private Map<String, Beer> beerMap;

    {
        beerMap = new LinkedHashMap<String, Beer>();
    }

    @Override
    public String addBeer() {

        String[] uId = UUID.randomUUID().toString().split("-");
        beerMap.put(uId[0],new Beer(uId[1],"Beer1"));
        return "Beer added with Id : "+uId[1] ;
    }

    @Override
    public Collection<Beer> getAllBeers() {
        Collection c = beerMap.entrySet();
        return c;
    }
}

