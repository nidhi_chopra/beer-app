package org.example.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Builder
public class Beer {

    private String beerId;
    private String beerName;

    @Override
    public String toString() {
        return "Beer id --> "+getBeerId() + " and Beer Name is --> "+getBeerName();
    }
}
